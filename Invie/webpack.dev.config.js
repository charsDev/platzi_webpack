const path = require('path');


module.exports = {
  entry: {
    invie: path.resolve(__dirname, 'src/index.js'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js'
  },
  devServer: {
    port: 9000
  },
  module: {
    rules: [
      {
        // test: que tipo de archivo quiero reconocer,
        // use: que loader se va a encargar del archivo
        // Damos soporte a .js y .jsx (react)
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          // En este caso cambia la forma de usar el use ya que 
          // vamos a personalizarlo 
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react', 'stage-2'],
            // el stage-2 es para que nos funcione el spread-operator, 
            //los 3 puntos (...algo), ya que esta caracteristica todavia se
            //encuentra en el stage-2 es decir todavia no esta implementado por default
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1000000,
              //El fallback es para cuando las imagenes pesan mas del limit
              // entonces se mueven a otro archivo con el nombre del valor de name
              fallback: 'file-loader',
              name: "images/[name].[hash].[ext]",
            }
          }
        ]
      }
    ]
  }
}
