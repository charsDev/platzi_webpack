const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: {
    home: path.resolve(__dirname, 'src/js/index.js'),
    precios: path.resolve(__dirname, 'src/js/precios.js'),
    contacto: path.resolve(__dirname, 'src/js/contacto.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    // hacemos nombre dinamico [name]
    filename: 'js/[name].js'
  },
  mode: 'development',
  module: {
    rules: [
      // Aquí van los loaders
      {
        // test: que tipo de archivo quiero reconocer,
        // use: que loader se va a encargar del archivo
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          // ['style-loader','css-loader'],
          // fallback: 'style-loader',
          use: "css-loader"
        })
      }
    ]
  },
  plugins: [
    // aqui van los plugins
    // new ExtractTextPlugin("css/styles.css")

    // esta manera de poner [name] es para que el archivo .css tome el 
    // nombre del archivo padre, en este caso es main por ser el entry
    new ExtractTextPlugin("css/[name].css")
  ]
}