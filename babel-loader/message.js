import renderToDOM from './render-to-dom'
import makeMessage from './make-message'

const waitTime = new Promise((resolve, reject)=> {
  setTimeout(() => {
    resolve('Han pasado 3 segundos, yes');
  }, 3000);
})

module.exports = {
  firstMessage: 'Hola mundo desde un modulo',
  delayedMessage: async() => {
    const message = await waitTime;
    console.log(message);
    // const element = document.createElement('p')
    // element.textContent = message;
    renderToDOM(makeMessage(message))
  },
}