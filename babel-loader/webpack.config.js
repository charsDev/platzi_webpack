const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  entry: path.resolve(__dirname, 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  mode: 'development',
  module: {
    rules: [
      // Aquí van los loaders
      {
        // test: que tipo de archivo quiero reconocer,
        // use: que loader se va a encargar del archivo
        test: /\.js$/,
        use: {
          // En este caso cambia la forma de usar el use ya que 
          // vamos a personalizarlo
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        },
      },
      {
        // test: que tipo de archivo quiero reconocer,
        // use: que loader se va a encargar del archivo
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          // ['style-loader','css-loader'],
          // fallback: 'style-loader',
          use: "css-loader"
        })
      }
    ]
  },
  plugins: [
    // aqui van los plugins
    // new ExtractTextPlugin("css/styles.css")

    // esta manera de poner [name] es para que el archivo .css tome el 
    // nombre del archivo padre, en este caso es main por ser el entry
    new ExtractTextPlugin("css/[name].css")
  ]
}