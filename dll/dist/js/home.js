/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./dll/src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dll/src/css/estilos.css":
/*!*********************************!*\
  !*** ./dll/src/css/estilos.css ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./dll/src/css/estilos.css?");

/***/ }),

/***/ "./dll/src/css/main.less":
/*!*******************************!*\
  !*** ./dll/src/css/main.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./dll/src/css/main.less?");

/***/ }),

/***/ "./dll/src/css/teacher.styl":
/*!**********************************!*\
  !*** ./dll/src/css/teacher.styl ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./dll/src/css/teacher.styl?");

/***/ }),

/***/ "./dll/src/css/teachers.scss":
/*!***********************************!*\
  !*** ./dll/src/css/teachers.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// removed by extract-text-webpack-plugin\n\n//# sourceURL=webpack:///./dll/src/css/teachers.scss?");

/***/ }),

/***/ "./dll/src/js/components/teacher.js":
/*!******************************************!*\
  !*** ./dll/src/js/components/teacher.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _react = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n\nvar _react2 = _interopRequireDefault(_react);\n\n__webpack_require__(/*! ../../css/teacher.styl */ \"./dll/src/css/teacher.styl\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction Teacher(props) {\n  return _react2.default.createElement(\n    'li',\n    { className: 'teacher' },\n    _react2.default.createElement(\n      'p',\n      null,\n      props.name,\n      ' ',\n      _react2.default.createElement(\n        'a',\n        { href: 'https://twitter/' + props.twitter },\n        '@',\n        props.twitter\n      )\n    )\n  );\n}\n\nexports.default = Teacher;\n\n//# sourceURL=webpack:///./dll/src/js/components/teacher.js?");

/***/ }),

/***/ "./dll/src/js/components/teachers.js":
/*!*******************************************!*\
  !*** ./dll/src/js/components/teachers.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nvar _react = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n\nvar _react2 = _interopRequireDefault(_react);\n\nvar _teacher = __webpack_require__(/*! ./teacher.js */ \"./dll/src/js/components/teacher.js\");\n\nvar _teacher2 = _interopRequireDefault(_teacher);\n\n__webpack_require__(/*! ../../css/teachers.scss */ \"./dll/src/css/teachers.scss\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nvar Teachers = function (_Component) {\n  _inherits(Teachers, _Component);\n\n  function Teachers() {\n    _classCallCheck(this, Teachers);\n\n    return _possibleConstructorReturn(this, (Teachers.__proto__ || Object.getPrototypeOf(Teachers)).apply(this, arguments));\n  }\n\n  _createClass(Teachers, [{\n    key: 'render',\n    value: function render() {\n      return _react2.default.createElement(\n        'ul',\n        { className: 'teachers' },\n        this.props.data.map(function (teacherData) {\n          return _react2.default.createElement(_teacher2.default, _extends({}, teacherData, { key: teacherData.twitter }));\n        })\n      );\n    }\n  }]);\n\n  return Teachers;\n}(_react.Component);\n\nexports.default = Teachers;\n\n//# sourceURL=webpack:///./dll/src/js/components/teachers.js?");

/***/ }),

/***/ "./dll/src/js/index.js":
/*!*****************************!*\
  !*** ./dll/src/js/index.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n__webpack_require__(/*! ../css/estilos.css */ \"./dll/src/css/estilos.css\");\n\nvar _teachers = __webpack_require__(/*! ./teachers.json */ \"./dll/src/js/teachers.json\");\n\nvar _teachers2 = _interopRequireDefault(_teachers);\n\nvar _react = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n\nvar _react2 = _interopRequireDefault(_react);\n\nvar _reactDom = __webpack_require__(/*! react-dom */ \"./node_modules/react-dom/index.js\");\n\nvar _teachers3 = __webpack_require__(/*! ./components/teachers.js */ \"./dll/src/js/components/teachers.js\");\n\nvar _teachers4 = _interopRequireDefault(_teachers3);\n\n__webpack_require__(/*! ../css/main.less */ \"./dll/src/css/main.less\");\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n(0, _reactDom.render)(_react2.default.createElement(_teachers4.default, { data: _teachers2.default.teachers }), document.getElementById('container'));\n\n//# sourceURL=webpack:///./dll/src/js/index.js?");

/***/ }),

/***/ "./dll/src/js/teachers.json":
/*!**********************************!*\
  !*** ./dll/src/js/teachers.json ***!
  \**********************************/
/*! exports provided: teachers, default */
/***/ (function(module) {

eval("module.exports = {\"teachers\":[{\"name\":\"Freddy Vega\",\"twitter\":\"freddier\"},{\"name\":\"Sacha Lifycz\",\"twitter\":\"sachalifs\"},{\"name\":\"Julian Duque\",\"twitter\":\"julian_duque\"},{\"name\":\"Leonidas Esteban\",\"twitter\":\"LeonidasEsteban\"}]};\n\n//# sourceURL=webpack:///./dll/src/js/teachers.json?");

/***/ }),

/***/ "./node_modules/react-dom/index.js":
/*!******************************************************************************!*\
  !*** delegated ./node_modules/react-dom/index.js from dll-reference modules ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = (__webpack_require__(/*! dll-reference modules */ \"dll-reference modules\"))(\"./node_modules/react-dom/index.js\");\n\n//# sourceURL=webpack:///delegated_./node_modules/react-dom/index.js_from_dll-reference_modules?");

/***/ }),

/***/ "./node_modules/react/index.js":
/*!**************************************************************************!*\
  !*** delegated ./node_modules/react/index.js from dll-reference modules ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = (__webpack_require__(/*! dll-reference modules */ \"dll-reference modules\"))(\"./node_modules/react/index.js\");\n\n//# sourceURL=webpack:///delegated_./node_modules/react/index.js_from_dll-reference_modules?");

/***/ }),

/***/ "dll-reference modules":
/*!**************************!*\
  !*** external "modules" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = modules;\n\n//# sourceURL=webpack:///external_%22modules%22?");

/***/ })

/******/ });