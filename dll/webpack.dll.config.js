const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const webpack = require('webpack')

module.exports = {
  entry:{
    modules: ['react','react-dom'],
  }, 
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
    // Utilizamso library para que el segundo archivo que va a utilizar a este archivo lo encuentre (dist/js/module.js)
    // [name] = modules <-- esto viene del entry
    library: '[name]'
  },
  mode: 'development',
  plugins: [
    new webpack.DllPlugin({
      // Este name es para saber que nombres vamos a utilizar del otro lado como referencias, este valor de name pasa al JSON
      name: "[name]",
      path: path.join(__dirname, "[name]-manifest.json")
    })
  ]
}

/* No utilizamos Loaders en este archivo ya que react ya utiliza codigo
que los navegadores ya entienden.
Si tuilizamos plugins para poder utilizar el webpacl.DllPlugin */
 