# Introducción y conceptos basicos de Webpack
## __¿Qué podemos hacer con Webpack?__
* *Webpack Module Bundler for modern JS applications.* Es un empaquetador de modulos para aplicaciones modernas de JS
* Modulos en JS se pueden manejar de multiples formas:
  * AMD (Asynchronous module definition). La libreria mas famosa que lo utiliza es require.js que sirve para cargar modulos de forma asincrona
  * CommonJS (Sistema de modulos e node.js). Igual que AMD solo que en lugar de hacer muchas peticiones solo hace una
* ¿Por que usar __webpack__? Por que tiene AMD + CommonJS
* *Entry Points* Modulo principal, de donde se importaran los demas modulos. Este es el archivo que leera __webpack__ para generar el bundle
* *Output* El archivo final, su ubicación y nombre
* *Loaders* Nos ayudara a cargar todo tipo de formato de archivos:  
Imagenes (jpg, png, gif)  
Fuentes personalizadas o iconos  
Dialectos (coffescript, stylus, sass, jsx)
* *Plugins* Nos ayudaran a extender las caracteristicas de webpack, ejemplo, uglify para comprimir archivos en pedazos mas pequeños paa que nuestra app cargue mas rapido

## __Instalación del entorno__
* Instalamos node
* Empezamos un npm init
* Instalamos webpack y wepack-cli como --save-dev
* Para checar lo instalado en un entorno utilizamos:  
npm list webpack

## __Creando el primer bundle__
* Creamos nuestro archivo index.js(Entry point)
* Creamos script BUILD para que podamos correr el comando webpack y poder crear el bundle.js
* Corremos con npm run build

## __Creando un webpack.config__
* Creamos un archivo webpack.config.js el cual contiene todos los datos para setear el entry y el output, asi mismo como el modo en el cual se esta trajando en el archivo (produccion, desarrollo)
* Este archivo para que funcione tiene que ser declarado como script en el package.json (corre con npm run build:local)
* El archivo bundle lo crea en la carpeta dist

## __Cargando configuración de rutas relativas__
* Parecido al tema anterior
* Creamos una carpeta que se llama external para verificar las rutas realtivas con *path*
* *resolve* funciona para que en el caso de windows las barras de las direcciones son invertidas, este comando nos ayuda a fixearlo
* Se creo otro script en el package.json

## __Loaders__
* Los loaders se cargan dentro de un key llamado *__rules__* que se encuentran dentro del key llamado *__module__*
* Dentro de rules es donde van los loaders
  * __*test*__ Sirve para decir que tipo de archivos queremos utilizar (exp. reg)
  * __*use*__ Aqui le decimos que loaders vamos a utilizar. En el caso de un solo loader pueden usarse comillas, dos o mas deben ser en un array []
* __css-loader__ cargar archivos css dentro de JS e interpretarlos
* __style-loader__ incorpora los estilos que están en la pila de estilos al index.html 
* OJO EL ORDEN DE LOS LOADERS AFECTA SU FUNCIONAMIENTO
* Creamos otro build (build:css), el flag --config indica el archivo de configuracion que debe utilizar (webpack.config.js)
* Importamos los estilos en el index.js (entry point) gracias a css-loader
* Llamamos al script dist/bundle.js desde el archivo index.html

## __Plugins__
* Creamos un script (build:extract:css) para correr nuestra tarea
* Instalamos extract-text-webpack-plugin --save-dev
* Este plugin lo que hara sera sacar el archivo css de manera externa y no inyectado como lo hace el loader
* Los plugins van dentro del key con el mismo nombre
* Se llaman asi new ExtractTextPlugin("css/style.css"), donde lo que va en parentesis es el nombre del archivo resultante
* Es necesario que en los __*loaders*__ en la parte de __*use*__ tambien se agrege el uso del plugin
* DIFERENCIA ENTRE LOADERS Y PLUGINS  
__*Loaders*__ si o si van a ser para interpretar tipos de archivos  
__*Plugins*__ pueden hacer cosas extras o más generales. Por ejemplo comprimir archivos de imagen, usar uglify.

## __Múltiples entry points__
* Creamos un script (build:multiples) para correr tarea
* Creamos la carpeta src/js y dentro de esta los archivos js correspondientes a cada pagina
* En el archivo .config cambiamos el entry, de un solo archivo lo modificamos para varios entrys
* Tambien modificamos el archivo de salida, de mamera que obtenga el nombre dinamicamente
* Creamos una carpeta src/css donde estaran los archivos css
* Modificamos el import en los archivos js para no causar problmas con la ruta
* Creamos archivos html correspondientes, cargamos los scripts que le correspondan segun sus nombres, asi mismo como el archivo css que le corresponda

## __Iniciando un servidor de desarrollo - Webpack DevServer__
* Creamos un script (build:dev) el cual tiene un flag --watch que observara los cambios en los archivos
* Para actualizar la pagina tenemos que hacerlo manualmente
* Para que detecte cambios y actualice en automatico utilizaremos webpack-dev-server, lo instalamos como -D (--save-dev)
* Cambiamos el script (build:dev) para que ahora utilice el webpack-dev-server, quitamos el --watch al final
* En el index.html cambiamos el script por el bundle que arroja el webpack devserver, ya que este se refresca cada que hacemos cambios
* Con el key devServer podemos agregar configuraciones personales, como por ejemplo cambiar el puerto. Hay muchas mas [dev-server](https://webpack.js.org/configuration/dev-server/)

# Personalizando la configuración con loaders y plugins
## __Soporte de EcmaScript__
* __*Babel:*__ Compila codigo JS de forma que el navegador lo entienda
* Los loaders que usaremos son los siguientes:  
  * __*babel-loader:*__ Da soporte a los archivos JS  
  * __*core-babel:*__ Inidca como se va a transpilar, transformar, compilar para que sea codigo que entienda el navegador  
  * __*babel-preset-env:*__ Para indicar que estandar de EmacScript utilizaremos. Se cambia el env por (es2015, es2016, etc)
* Creamos un script (build:babel) para correr nuestra tarea
* Instalamos los loaders (babel-loader, core-babel, babel-preset-env)
* Agregamos al archivo webpack.config otro loader, el cual se encargara de poder manejar los archivos JS (babel-loader) y lo personalizamos para que use el preset es20015
* Importamos el firstMessage del archivo message.js para poder usarlo en el index.js

## __Utilizando JS moderno en nuestro código__
* En este capitulo vimos el uso del loader de babel para importar archivos JS entre ellos mismos
* Creamos una funcion asincrona delayedMessage que espera 3 segundos e imprime un mensaje
* Para esto creamos un archivo render-to-dom.js para que pueda ser utilizado para renderear los mensajes que queramos, de una manera modular
* Tambien creamos un archivo make-message.js el cual asu vez se encarga de crear el mensaje que le pasemos y asi poder pasarlo al render-to-dom
* Se llama destructing a la manera de importar solo ciertas partes de un archivo a otro (import {} form algo.js)

## __Soporte de Imágenes en Webpack__
* Instalamos url-loader
* Creamos un script (build:image)
* Configuramos loaders en el webpack.gonfig agregandole otro loader (url-loader) en este caso para imagenes con un limite 100 000 bytes
* Seteamos la imagen en el index.js y la appendeamos
* En los estilos.css ponemos de background la imagen para notar que tambien usa base64 mediante css
* Cambiamos los estilos en el index.html para que tome los que se llaman main.css

## __Soporte de fuentes en Webpack__
* Creamos script (build:fonts)
* Descargamos la fuente open-sans de fontsquirrel
* Pegamos el css del open-sans en nuestro archivo css y usamos la fuente
* Creamos carpeta fonts
* En webpack.config modificamos el url-loader para que tambien reciba archivos de fuentes
* Reestructuramos las carpetas para una mayor organización, se cambiaron las rutas

## __Soporte de vídeos__
* Creamos script (build:video)
* En webpack.config creamos otro loader para poder utilizar los videos
* Como opciones le pusimos un limite de tamaño al archivo
* Le pusimos un name que sera el nombre de la carpeta donde estaran estos videos en la carpeta dist, con los comodines
  * __*[name]*__ Para que tenga el mismo nombre del archivo original
  * __*[hash]*__ Crea un hash para que no se repitan los nombres
  * __*[ext]*__ Toma la extension del archivo original
* Para que la carpeta videos que creamos en el loader, pueda ser reconocida en el navegador necesitamos anteponerle el (dist/), eso lo hacemos en output en la opcion publicPath
* Ya que algunos videos sobrepasan el 100000 bytes de tamaño, es necesario instalar la dependencia file-loader para mover los archivos

## __Soportando archivos JSON__
* Creamos script (build:json)
* Creamos el archivo json (teachers.json)
* En el index.js rendereamos la lista de maestros del json
* OJO NO USAMOS JSON-LOADER YA QUE DE WEBPACK 4 HACIA ARRIIBA YA LO TIENE INCLUIDO

## __Configuración para React.js__
* Instalamos las dependecias:
  * npm i -D babel-preset-react
  * npm i -S react react-dom
* Creamos script (build:react)
* En esta seccion vimos como utilizar los loades para react, el cual se carga el los presets de babel-loader
* Se hicieron cambios en los archivos JS pero eso solo para efecto de REACT (Curso mas adelante)

## __Estilos con Sass__
* Creamos el script (build:sass)
* Instalamos sass-loader y node-sass
* Usamos el loader de sass en el webpack.config
* Creamos el archivo teacher.scss, que es SASS
* Importamos el archivo en el teachers.js

## __Estilos con Stylus__
* Instalamos el stylus-loader para webpack y el stylus que es el core para mover stylus
* Creamos script (build:stylus)
* Agregamos el loader en el webpack.config, le pasamos unos mixins en las opciones:
  * __*nib*__ conjunto mixins de prefijos para navegadores viejos, entiendan el codigo bien
  * __*rupture*__ mixins para dar soporte a mediaquerys de una forma mas sencilla
* Tambien pusimos en el import las rutas de los mixins, el simbolo ~ es un alias que señala a la carpeta de node_modules
* Creamos archivo teacher.styl
* Utiliamos +below que es la manera de rupture de manejar los media querys
* Importamos en teacher.js

## __Estilos con Less__
* Instalar el loader (less-loader) y el core (less)
* Creamos script (build:less)
* Agregamos el loader (less-loader) al webpack.config y le pasamos opciones
* Creamos archivo main.less
* Llamamos al archivo main.less en el index.js

## __Estilos con PostCSS__
* Instalamos el loader () y el core (PostCSS)
* Creamos script (build:postcss)
* En el webpack.config modificamos el css-loader para que tambien funcione con postcss-loader
* Le pasamos opciones como los modulos y que puede trabajar importando Loaders, en este caso postcss-loader
* Con PostCSS siempre tenemos que tener un archivo de configuracion (postcss.config.js) en el cual pasaremos los plugins que utilizaremos en postcss (postcss-cssnext)

# Conceptos avanzados de Webpack
## __Prevenir código duplicado__
* En este capitulo veremos como eliminar codigo innecesario en el bundle.js
* Creamos el script (build:prevent)
* Creamos otro acrchivo contact.js que importa lo mismo que index.js. Esto con el fin de ver como el codigo duplicado se guarda en un archivo con webpack
* En el webpack.config cambiamos los entry y el output (varios) y agregamos algo nuevo de webpack 4 que se llama optimization
* Despues de correr el script vemos como se crea un archivo llamado common.js
* Debemos de importar los scripts en el index.html

## __Eligiendo dependencias comunes__
* Creamos el script (build:vendor)
* En el webpack.config agregamos vendor a los entry, esto para decirle a webpack cuales son las dependecias que queremos en un archivo aparte (vendor.js)
* Modificamos el optimization
* Cambiamos el vendor en index.html
* [optimization.splitChunks](https://webpack.js.org/plugins/split-chunks-plugin/)

## __Optimizando el paquete de dependencias comunes__
* En conclusion sacar las dependencias que no cambiaran a un archivo, el cual solo se compilara una vez (webpack.dll.config) y el archivo que si cambiara (webpack.config) se referira a este para utilizar esas dependecias
* Creamos dos scripts:
  * "buil:dll" llena el archivo webpack.dll.config.js , que creamos previamente, el cual nos genera el paquete de archivos comunes, que solo vamos a transpilar una sola vez. Este archivo tiene una configuracion doferente
  * "build:dll:src" nos sirve para manipular el webpack.config.js (como lo haceiamos anteriormente) y lo vamos a compilar las veces que sean necesarias (actualizar codigo). Este hara referencia al archivo webpack.dll.config
* __*DllPlugin:*__ nos sirve para generar nuestro bundle de dependecias comunes y luego darle cierta configuracion mediante un archivo de configuracion externo. Luego nuestro archivo webpack.config hara referencia a este archivo y no lo compilara cada vez que hagamos un build. Exporta un archivo JSON 
* __*DllReferencePlugin:*__ Es la manera en que el archivo principal (webpack.config) tomara al archivo de dependencias comunes (webpack.dll.config). Utiliza el archivo JSON que crea el DllPlugin

## __Enlazando un Dynamic Link Library (DLL)__
* Ya tenemos nuestro paquete de dependecias comunes (DLL) y lo que haremos aqui sera enlazarlo con los entry points originales (home, contacto, etc) para que lo consuman. Esto lo hacemos desde el archivo de config original (webpack.config)
* En nuestro archivo webpack.config, en los plugins, agregamos uno nuevo el cual sera el DllReferencePlugin que como su nombre lo indica, hace referencia al archivo donde tenemos nuestas dependecias comunes, en este caso react y react-dom y asi no tener que compilarlos cada vez que hagamos un build. Este plugin requiere el archivo json el cual creamos con el dllplugin en el tema anterior y se pasa a la varaible manifest
* En este mismo archivo, en la parte de entry, eliminamos o comentamos la parte de vendor, ya que no la necesitaremos por que hacemos referencia al archivo de dependecias comunes en el cual se encuentran dichos archivos

## __Cargando módulos asincronamente__
* Cargar dependencias dependiendo cuando visitemos una pagina o se cumpla algo
* Creamos nuestro script (build:dynamic)
* Creamos un buton en el index.html
* Creamos un archivo alerta.js el cual sera el modulo que cargaremos asincronamente
* En el index.js asignamos el buton a la constante $button y luego le agregamos un envento click el cual tendra una funcion asincrona la cual importara el alert.js en una variable (deconstructing) que sera el defaul:alerta que exportamos en el archivo alerta.js
* En el babel-loader agregamos el plugin syntax-dynamic-import el cual nos sirve para que al hacer el await import en el archivo index.js este no cause problemas ya que babel tiene import como palabra reservada
* En el output seteamos una publicPath donde esta la carpeta dist
* Con la variable chunkFilename pasamos la ruta donde estaran los archivos chunk que contienen la funcion asincrona y podemos modificar la estrucutra de su nombre, por ejemplo un id automatico y un hash (chunkhash)

## __Llevando un proyecto real a Webpack - preparación del entorno__
* Solo preparamos el entorno, todavia no hacemos nada con webpack

## __Llevando un proyecto real a Webpack - configuración para entornos de desarrollo__
* OJO -- Por problemas se cambiaron la carpeta invie que nosotros hicimos por la del repo de Leonidas 
* Creamos el archivo webpack.dev.config
* Creamos eel script (build:dev) en el package.json
* Verificamos los archivos que tenemos (.js, .jsx, .svg, etc) para saber que dependencias de desarrollo necesitamos instalar (loaders y plugins) en este caso son: babel-core babel-loader babel-preset-es2015 babel-preset-react style-loader css-loader url-loader file-loader webpack webpack-dev-server babel-preset-stage-2
* Seteamos los loaders en el archivo webpack.dev.config.js
* Seteamos el puerto donde correra nuesta pagina (devServer)
* Seteamos el entry y el output
* En el archivo index.html seteamos el script con el src el cual apunta a nuestro puerto de nuestro servidor y al archivo que se genera en el dev

## __Configuración para entornos de producción__
* Seguimos en la carpeta invie
* Instalamos el plugin extract-text-webpack-plugin
* Requerimos el plugin extract-text-webpack-plugin en el webpack.config y lo utilizamos en la seccion plugins
* Cambiamos el loader de css, utilizamos el ExtractTextPlugin y lo configuramos para que utilice el css-loader
  * Minimize: como su nombre lo indica, miniza (quita espacios) el css para que el navegador lo puede cargar mas rapido
  * Imports: permite que se puede utilizar modulos en el css y al final tener un solo archivo --- SE ELIMINA
* En el output del archivo .config agregamos:
  * PublicPath: la ruta donde el sistema buscara los archivos que creemos
  * ChunkFilename: nombre que tendran los pedazos de codigo (dynamics imports) dentro de la carpeta js
* Creamos el script (build:prod) y al correr este script se creara una carpeta dist con carpetas js, css(con el archivo minificado) y images, con sus respectivos archivos
* Creamos una varible plugins, la cual tendra todos los plugins que necesitaremos y solo la mencionaremos en la seccion de plugins
* En total seran 3 scripts que creamos (build, build:local, build:prod)
* En los scripts (build:local y build:prod) pasamos la variable de entorno segun sea el caso
* Debido a que estamos pasando argumentos a nuestra configuracion, es necesario que nuestros modulos.exports sean una funcion y que reciban como parametro la variable de entorno (env) y dentro de esta funcion ira nuesta variable de plugins y nuestra condicion
* En el webpack.config creamos una condicional para saber que pasara en caso de que sea local o produccion la variable de entorno
* En el caso que la variable de entorno sea igual a __*produciton*__ la variable plugins hara un push el cual limpara el archivo. Esto lo hacemos con el plugin CleanWebpackPlugin
* CleanWebpackPlugin recibe dos parametros:
  * El primero indica el nombre de la carpeta a limpiar
  * El segundo alguna configuracion, en este caso de donde va a empezar a buscar las carpetas (dentro de dist), mediante la variable root
* En el script (build:prod) si le pasamos el flag -p hace minificacion del archivo js/invie(hash).js
* El hash en los archivos es una forma de limpiar el cache en los navegadores
* Cambiamos el script y el css del html
* Hasta hoy 12/julio/2018 no encontre como hacer que las imagenes de css y las de html se logren ver al mismo tiempo

