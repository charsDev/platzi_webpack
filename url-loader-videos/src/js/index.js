import '../css/estilos.css'
import {firstMessage, delayedMessage} from './message'
import platziImg from '../images/platzi.png'
import videoPlatzi from '../videos/video_uno.mp4'

document.write(firstMessage)
delayedMessage()

const img = document.createElement('img')
img.setAttribute('src', platziImg)
img.setAttribute('width', 50)
img.setAttribute('height', 50)
document.body.appendChild(img)

const video = document.createElement('video')
video.setAttribute('src', videoPlatzi)
video.setAttribute('width', 480)
video.setAttribute('autoplay', true)
video.setAttribute('controls', true)
document.body.appendChild(video)